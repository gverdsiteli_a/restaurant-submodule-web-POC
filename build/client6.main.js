(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["client6"],{

/***/ "./src/SearchField.client.tsx":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = SearchField;

var _react = __webpack_require__("./node_modules/react/index.js");

var _LocationContext = __webpack_require__("./src/LocationContext.client.tsx");

var _Spinner = _interopRequireDefault(__webpack_require__("./src/Spinner.tsx"));

var _jsxRuntime = __webpack_require__("./node_modules/react/jsx-runtime.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function SearchField() {
  const [text, setText] = (0, _react.useState)('');
  const [startSearching, isSearching] = (0, _react.unstable_useTransition)(false);
  const [, setLocation] = (0, _LocationContext.useLocation)();
  return /*#__PURE__*/(0, _jsxRuntime.jsxs)("form", {
    className: "search",
    role: "search",
    onSubmit: e => e.preventDefault(),
    children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("label", {
      className: "offscreen",
      htmlFor: "sidebar-search-input",
      children: "Search for a note by title"
    }), /*#__PURE__*/(0, _jsxRuntime.jsx)("input", {
      id: "sidebar-search-input",
      placeholder: "Search",
      value: text,
      onChange: e => {
        const newText = e.target.value;
        setText(newText);
        startSearching(() => {
          setLocation(loc => ({ ...loc,
            searchText: newText
          }));
        });
      }
    }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_Spinner.default, {
      active: isSearching
    })]
  });
}

/***/ }),

/***/ "./src/Spinner.tsx":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = Spinner;

var _jsxRuntime = __webpack_require__("./node_modules/react/jsx-runtime.js");

function Spinner({
  active = true
}) {
  return /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
    className: ['spinner', active && 'spinner--active'].join(' '),
    role: "progressbar",
    "aria-busy": active ? 'true' : 'false'
  });
}

/***/ })

}]);
//# sourceMappingURL=client6.main.js.map