(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["client7"],{

/***/ "./src/SidebarNote.client.tsx":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = SidebarNote;

var _react = __webpack_require__("./node_modules/react/index.js");

var _LocationContext = __webpack_require__("./src/LocationContext.client.tsx");

var _jsxRuntime = __webpack_require__("./node_modules/react/jsx-runtime.js");

function SidebarNote({
  id,
  title,
  children,
  expandedChildren
}) {
  const [location, setLocation] = (0, _LocationContext.useLocation)();
  const [startTransition, isPending] = (0, _react.unstable_useTransition)();
  const [isExpanded, setIsExpanded] = (0, _react.useState)(false);
  const isActive = id === location.selectedId; // Animate after title is edited.

  const itemRef = (0, _react.useRef)(null);
  const prevTitleRef = (0, _react.useRef)(title);
  (0, _react.useEffect)(() => {
    if (title !== prevTitleRef.current) {
      prevTitleRef.current = title;
      itemRef.current.classList.add('flash');
    }
  }, [title]);
  return /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
    ref: itemRef,
    onAnimationEnd: () => {
      itemRef.current.classList.remove('flash');
    },
    className: ['sidebar-note-list-item', isExpanded ? 'note-expanded' : ''].join(' '),
    children: [children, /*#__PURE__*/(0, _jsxRuntime.jsx)("button", {
      className: "sidebar-note-open",
      style: {
        backgroundColor: isPending ? 'var(--gray-80)' : isActive ? 'var(--tertiary-blue)' : '',
        border: isActive ? '1px solid var(--primary-border)' : '1px solid transparent'
      },
      onClick: () => {
        startTransition(() => {
          setLocation(loc => ({
            selectedId: id,
            isEditing: false,
            searchText: loc.searchText
          }));
        });
      },
      children: "Open note for preview"
    }), /*#__PURE__*/(0, _jsxRuntime.jsx)("button", {
      className: "sidebar-note-toggle-expand",
      onClick: e => {
        e.stopPropagation();
        setIsExpanded(!isExpanded);
      },
      children: isExpanded ? /*#__PURE__*/(0, _jsxRuntime.jsx)("img", {
        src: "chevron-down.svg",
        width: "10px",
        height: "10px",
        alt: "Collapse"
      }) : /*#__PURE__*/(0, _jsxRuntime.jsx)("img", {
        src: "chevron-up.svg",
        width: "10px",
        height: "10px",
        alt: "Expand"
      })
    }), isExpanded && expandedChildren]
  });
}

/***/ })

}]);
//# sourceMappingURL=client7.main.js.map