(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["client2"],{

/***/ "./src/EditButton.client.tsx":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = EditButton;

var _react = __webpack_require__("./node_modules/react/index.js");

var _LocationContext = __webpack_require__("./src/LocationContext.client.tsx");

var _jsxRuntime = __webpack_require__("./node_modules/react/jsx-runtime.js");

function EditButton({
  noteId,
  children
}) {
  const [, setLocation] = (0, _LocationContext.useLocation)();
  const [startTransition, isPending] = (0, _react.unstable_useTransition)();
  const isDraft = noteId == null;
  return /*#__PURE__*/(0, _jsxRuntime.jsx)("button", {
    className: ['edit-button', isDraft ? 'edit-button--solid' : 'edit-button--outline'].join(' '),
    disabled: isPending,
    onClick: () => {
      startTransition(() => {
        setLocation(loc => ({
          selectedId: noteId,
          isEditing: true,
          searchText: loc.searchText
        }));
      });
    },
    role: "menuitem",
    children: children
  });
}

/***/ })

}]);
//# sourceMappingURL=client2.main.js.map