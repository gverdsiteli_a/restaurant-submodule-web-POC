(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["client4"],{

/***/ "./src/NoteEditor.client.tsx":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = NoteEditor;

var _react = __webpack_require__("./node_modules/react/index.js");

var _reactServerDomWebpack = __webpack_require__("./node_modules/react-server-dom-webpack/index.js");

var _NotePreview = _interopRequireDefault(__webpack_require__("./src/NotePreview.tsx"));

var _Cache = __webpack_require__("./src/Cache.client.tsx");

var _LocationContext = __webpack_require__("./src/LocationContext.client.tsx");

var _jsxRuntime = __webpack_require__("./node_modules/react/jsx-runtime.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function NoteEditor({
  noteId,
  initialTitle,
  initialBody
}) {
  const refresh = (0, _Cache.useRefresh)();
  const [title, setTitle] = (0, _react.useState)(initialTitle);
  const [body, setBody] = (0, _react.useState)(initialBody);
  const [location, setLocation] = (0, _LocationContext.useLocation)();
  const [startNavigating, isNavigating] = (0, _react.unstable_useTransition)();
  const [isSaving, saveNote] = useMutation({
    endpoint: noteId !== null ? `/notes/${noteId}` : `/notes`,
    method: noteId !== null ? 'PUT' : 'POST'
  });
  const [isDeleting, deleteNote] = useMutation({
    endpoint: `/notes/${noteId}`,
    method: 'DELETE'
  });

  async function handleSave() {
    const payload = {
      title,
      body
    };
    const requestedLocation = {
      selectedId: noteId,
      isEditing: false,
      searchText: location.searchText
    };
    const response = await saveNote(payload, requestedLocation);
    navigate(response);
  }

  async function handleDelete() {
    const payload = {};
    const requestedLocation = {
      selectedId: null,
      isEditing: false,
      searchText: location.searchText
    };
    const response = await deleteNote(payload, requestedLocation);
    navigate(response);
  }

  function navigate(response) {
    const cacheKey = response.headers.get('X-Location');
    const nextLocation = JSON.parse(cacheKey);
    const seededResponse = (0, _reactServerDomWebpack.createFromReadableStream)(response.body);
    startNavigating(() => {
      refresh(cacheKey, seededResponse);
      setLocation(nextLocation);
    });
  }

  const isDraft = noteId === null;
  return /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
    className: "note-editor",
    children: [/*#__PURE__*/(0, _jsxRuntime.jsxs)("form", {
      className: "note-editor-form",
      autoComplete: "off",
      onSubmit: e => e.preventDefault(),
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("label", {
        className: "offscreen",
        htmlFor: "note-title-input",
        children: "Enter a title for your note"
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)("input", {
        id: "note-title-input",
        type: "text",
        value: title,
        onChange: e => {
          setTitle(e.target.value);
        }
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)("label", {
        className: "offscreen",
        htmlFor: "note-body-input",
        children: "Enter the body for your note"
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)("textarea", {
        id: "note-body-input",
        value: body,
        onChange: e => {
          setBody(e.target.value);
        }
      })]
    }), /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
      className: "note-editor-preview",
      children: [/*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
        className: "note-editor-menu",
        role: "menubar",
        children: [/*#__PURE__*/(0, _jsxRuntime.jsxs)("button", {
          className: "note-editor-done",
          disabled: isSaving || isNavigating,
          onClick: () => handleSave(),
          role: "menuitem",
          children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("img", {
            src: "checkmark.svg",
            width: "14px",
            height: "10px",
            alt: "",
            role: "presentation"
          }), "Done"]
        }), !isDraft && /*#__PURE__*/(0, _jsxRuntime.jsxs)("button", {
          className: "note-editor-delete",
          disabled: isDeleting || isNavigating,
          onClick: () => handleDelete(),
          role: "menuitem",
          children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("img", {
            src: "cross.svg",
            width: "10px",
            height: "10px",
            alt: "",
            role: "presentation"
          }), "Delete"]
        })]
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
        className: "label label--preview",
        role: "status",
        children: "Preview"
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)("h1", {
        className: "note-title",
        children: title
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_NotePreview.default, {
        title: title,
        body: body
      })]
    })]
  });
}

function useMutation({
  endpoint,
  method
}) {
  const [isSaving, setIsSaving] = (0, _react.useState)(false);
  const [didError, setDidError] = (0, _react.useState)(false);
  const [error, setError] = (0, _react.useState)(null);

  if (didError) {
    // Let the nearest error boundary handle errors while saving.
    throw error;
  }

  async function performMutation(payload, requestedLocation) {
    setIsSaving(true);

    try {
      const response = await fetch(`${endpoint}?location=${encodeURIComponent(JSON.stringify(requestedLocation))}`, {
        method,
        body: JSON.stringify(payload),
        headers: {
          'Content-Type': 'application/json'
        }
      });

      if (!response.ok) {
        throw new Error(await response.text());
      }

      return response;
    } catch (e) {
      setDidError(true);
      setError(e);
    } finally {
      setIsSaving(false);
    }
  }

  return [isSaving, performMutation];
}

/***/ }),

/***/ "./src/NotePreview.tsx":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = NotePreview;

var _TextWithMarkdown = _interopRequireDefault(__webpack_require__("./src/TextWithMarkdown.tsx"));

var _jsxRuntime = __webpack_require__("./node_modules/react/jsx-runtime.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function NotePreview({
  body
}) {
  return /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
    className: "note-preview",
    children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_TextWithMarkdown.default, {
      text: body
    })
  });
}

/***/ }),

/***/ "./src/TextWithMarkdown.tsx":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = TextWithMarkdown;

var _marked = _interopRequireDefault(__webpack_require__("./node_modules/marked/lib/marked.js"));

var _sanitizeHtml = _interopRequireDefault(__webpack_require__("./node_modules/sanitize-html/index.js"));

var _jsxRuntime = __webpack_require__("./node_modules/react/jsx-runtime.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const allowedTags = _sanitizeHtml.default.defaults.allowedTags.concat(['img', 'h1', 'h2', 'h3']);

const allowedAttributes = Object.assign({}, _sanitizeHtml.default.defaults.allowedAttributes, {
  img: ['alt', 'src']
});

function TextWithMarkdown({
  text
}) {
  return /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
    className: "text-with-markdown",
    dangerouslySetInnerHTML: {
      __html: (0, _sanitizeHtml.default)((0, _marked.default)(text), {
        allowedTags,
        allowedAttributes
      })
    }
  });
}

/***/ }),

/***/ 1:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 2:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 3:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 4:
/***/ (function(module, exports) {

/* (ignored) */

/***/ })

}]);
//# sourceMappingURL=client4.main.js.map