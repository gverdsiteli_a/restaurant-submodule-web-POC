FROM node:6.11.3
WORKDIR /web
COPY package.json .
RUN npm install --legacy-peer-deps && \
    npm install -g pm2
COPY . .
EXPOSE 3000

CMD ["pm2-runtime", "process.yml", "--only", "APP"]